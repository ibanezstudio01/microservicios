## Lerna

configurar lerna
--instalar lerna
yarn add -D lerna
--inicializar lerna
npx lerna init

## Husky / convention commits

--instalar husky
yarn add -D husky
--inicializar husky
npx husky-init && yarn
-- crear nueva condicion husky
npx husky add .husky/commit-msg "echo 'Conventional commit'"

--instalar conventional commits
yarn add -D commitizen
--inicializar conventional commits
npx commitizen init cz-conventional-changelog --yarn --dev --exact

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
